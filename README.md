# Django Poll App

## Installation
- First clone the repository using https/ssh:
  ```sh
  git clone {git:ssh/https}
  ```
- Install Virtual Environment:
  ```sh
  sudo apt-get install python3.10-venv
  ```
- Creating virtual env:
  ```sh
  python3 -m venv venv
  ```
- Activating virtual env:
  ```sh
  source venv/bin/activate
  ```
- Installing all the requirements for django poll app:
  ```sh
  pip install -r requirements.txt
  ```

<br>

## Running the App

- Go into src directory to run app:
  ```sh
  cd src
  ```
- Connected with **Postgresql** Database, so creation of same database with grant all permission with dbname, user, password all provided in the **src/my-site/settings.py** under DATABASE.
- To show migrations
  ```sh
  python manage.py makemigrations
  ```
- To complete migration
  ```sh
  python manage.py migrate
  ```
- Running the Poll App in port 8080
  ```sh
  python manage.py runserver 8080
  ```
- Go to the localhost:8080 from the browser to view the app
- Deactivate server with CONTROL+C
- Deactivate virtual env with:
  ```sh
  deactivate
  ```

## Login into Admin Panel

- Create superuser:
  ```sh
  python manage.py createsuperuser
  ```
- Enter username, email, password
- Go to localhost:8080/admin
- Enter username, password
- Go to localhost:8080/admin/polls to view the polls


## Screenshots

- Welcome Screen
![Welcome Screen](./screenshots/welcome.png)

- Home Screen
![Home Screen](./screenshots/home.png)

- Poll
![Poll](./screenshots/poll.png)

- Polls Result
![Polls Result](./screenshots/poll_result.png)

- Admin Panel
![Admin Panel](./screenshots/admin_page.png)

- Admin Panel Add Question
![Admin Panel Add Question](./screenshots/add_question.png)